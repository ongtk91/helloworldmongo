/**
 * View Models used by Spring MVC REST controllers.
 */
package com.grey.web.rest.vm;
